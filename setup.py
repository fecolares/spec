import setuptools

setuptools.setup(
  name='w3d_pyspec',
  version='1.0.0',
  author='Franco',
  author_email='fecolares@graff3d.com',
  description='Python spec helpers',
  long_description='',
  long_description_content_type='',
  url='https://gitlab.com/fecolares/spec',
  project_urls = {},
  license='MIT',
  packages=['w3d_pyspec'],
  install_requires=['jsonschema'],  
)